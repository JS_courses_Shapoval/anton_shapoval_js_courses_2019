//Создать функцию  compose которая выполняет композицию функций справа налево.
//compose(fn1, fn2, fn3, ... fnN)
//тоже самое что и  
//fn1(fn2(fn3(...fnN(1))))

function plusOne(x) {
	return x++;
}
function negative(x) {
	return -x;
}
const compose = (...args) => (...ar) => args.reduceRight((res, fn) => [fn(...res)], ar)[0];
const redirection = compose (plusOne, negative, Math.pow);
console.log(redirection(3, 4));

//Написать функцию curryN, которая каррирует N-е количество аргументов переданной функции
function curry(paramForCurry, ...args) {
	return paramForCurry.bind(this, ...args)
}
function printFullName(firstName, lastName, middleName) {
	return `${firstName} ${lastName} ${middleName}`
}
let curriedPrintFullName = curry(printFullName, "Anton", "Viktorovich", "Shapoval");
console.log(curriedPrintFullName());

//Написать функцию, что будет “глубоко” копировать объект. 
//То есть, должны сделать копию не только объекта, а и его вложенностей 

function copy(obj) {
  let copyObj = {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if ("object" === typeof obj[key] && obj[key] !== null)
      copyObj[key] = copy(obj[key]);
      else
      copyObj[key] = obj[key];
    }
  }

 return copyObj;
}

//Написать функцию которая будет каждую минуту выводить текущие время в формате hh:mm

const timeEveryOneMinute = () => {
	let Data = new Date();
	let Hour = Data.getHours();
	let Minutes = Data.getMinutes();
		console.log("Текущее время: " + Hour + ":" + Minutes + " ");
		setTimeout(timeEveryOneMinute, 1000 * 60);
};

timeEveryOneMinute();
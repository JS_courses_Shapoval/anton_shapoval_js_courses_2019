function Person(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
}

Person.prototype.checkAge = function () {
    const minAge = 0;
    const maxAge = 150;
    try {
        if (this.age < minAge) {
            throw `age is less than ${minAge}`;
        }
        if (this.age > maxAge) {
            throw `age is more than ${maxAge}`;
        }
    } catch (error) {
        throw new Error(error);
    }
};

Person.prototype.checkFirstName = function () {
    checkNameLength(this.firstName);
};

Person.prototype.checkLastName = function () {
    checkNameLength(this.lastName);
};

Person.prototype.checkFirstNameForLatin = function () {
    checkNameForLatin(this.firstName);
};

Person.prototype.checkLastNameForLatin = function () {
    checkNameForLatin(this.lastName);
};

Person.prototype.checkFullName = function () {
    this.checkFirstNameForLatin();
    this.checkLastNameForLatin();
};

Person.prototype.setFullName = function (fullName) {
    const splitName = fullName.split(' ');
    this.firstName = splitName[0];
    this.lastName = splitName[1];
    this.checkFullName();
};

Person.prototype.getFullName = function () {
    return this.firstName.toUpperCase() + ' ' + this.lastName.toUpperCase();
};

Person.prototype.introduce = function () {
    return `Hello! My name is ${this.getFullName()} and I am ${this.age}-years-old`;
};

function checkNameLength(name) {
    const minLengthForName = 3;
    const maxLengthForName = 20;
    try {
        if (name.length < minLengthForName) {
            throw `${name} is less than ${minLengthForName}`;
        }
        if (name.length > maxLengthForName) {
            throw `${name} is more than ${maxLengthForName}`;
        }
    } catch (error) {
        throw new Error(error);
    }
}

function checkNameForLatin(name) {
    const latinLeters = /^[a-zA-Z]+$/;
    try {
        if (!latinLeters.test(name)) {
            throw `the name ${name} must have only latin leters`;
        }
    } catch (error) {
        throw new Error(error);
    }
}

const person = new Person('Frank', 'Senatra', 149);
console.log(`${person.firstName} ${person.lastName} ${person.age}`);

console.log(person);
person.checkAge();
person.checkFirstName();
person.checkLastName();
person.checkFirstNameForLatin();
person.checkLastNameForLatin();
person.setFullName('Norman Reedus');
person.getFullName();
console.log(person.introduce());
console.log(person);

function Worker(firstName, lastName, age, experience, salary) {
    Person.apply(this, arguments);
    this.experience = experience;
    this.salary = salary;
}

Worker.prototype = Object.create(Person.prototype);
Worker.prototype.constructor = Worker;

Worker.prototype.setExperience = function (experience) {
    const minExperience = 1;
    const maxExperience = 50;
    try {
        if (experience < minExperience) {
            throw `experience is less than ${minExperience}`;
        }
        if (experience > maxExperience) {
            throw `experience is more than ${maxExperience}`;
        }
        this.experience = experience;
    } catch (error) {
        throw new Error(error);
    }
};

Worker.prototype.getExperience = function () {
    return this.experience;
};

Worker.prototype.setSalary = function (salary) {
    const minSalary = 1000;
    const maxSalary = 10000;
    try {
        if (salary < minSalary) {
            throw `salary is less than ${minSalary}`;
        }
        if (salary > maxSalary) {
            throw `salary is more than ${maxSalary}`;
        }
        this.salary = salary;
    } catch (error) {
        throw new Error(error);
    }
};

Worker.prototype.getSalary = function () {
    return this.experience;
};

Worker.prototype.getRemunerationRate = function () {
    const percent = 100;
    const oneToFive = 5;
    const sixToTen = 10;
    const elevenToTwenty = 20;
    const twentyOneToFiftin = 50;
    const experience = this.getExperience();
    switch (true) {
        case checkRange(experience, 1, 5):
            return experience * oneToFive / percent;
        case checkRange(experience, 6, 10):
            return experience * sixToTen / percent;
        case checkRange(experience, 11, 20):
            return experience *  elevenToTwenty/ percent;
        case checkRange(experience, 21, 50):
            return experience * twentyOneToFiftin / percent;
        default:
            return 0;
    }
};

function checkRange(experience, min, max) {
    return experience >= min && experience <= max;
}

const workMan = new Worker('Mark', 'Cukerberg', 23, 4, 4000);

console.log(workMan);
workMan.checkAge();
workMan.checkFirstName();
workMan.checkLastName();
workMan.checkFirstNameForLatin();
workMan.checkLastNameForLatin();
workMan.setExperience(22);
console.log(workMan.getExperience());
workMan.setSalary(1000);
console.log(workMan.getSalary());
console.log('remuneration rate: ' + workMan.getRemunerationRate());
console.log(workMan);
const makeRandomFn = function() {
const anyNumberFromArgs = [].concat.apply([],arguments);
	return function() {
    	return anyNumberFromArgs[Math.random() * anyNumberFromArgs.length|0]
	}
};
const getRandomNumber = makeRandomFn(1, 2, 100, 34, 45, 556, 33)
console.log(getRandomNumber());
console.log(getRandomNumber());
console.log(getRandomNumber());
function makeRandomFn(getRandomNumber) {
    return function() {
	    return getRandomNumber[Math.floor(Math.random() *getRandomNumber.length)];
	}
}
const getRandomNumber = makeRandomFn([1, 2, 100, 34, 45, 556, 33])
console.log(getRandomNumber()); 
console.log(getRandomNumber());
console.log(getRandomNumber());
console.log(getRandomNumber()); 

// Task 1.
// Создайте функцию, которая на вход получаете строку "a.b.c.d",
// а на выходе возвращает объект: { a: { b: { c: { d: null } } } }
function strToObject(str) {
	return str.split('.').reduceRight((leter, index) => ({ [index]: leter }), null);
}
console.log(strToObject("q.w.e.r"));

// Task 2
// Написать функцию, которая находит пересечение двух массивов
// Например - результатом вызовом findEquals([2, 6, 3, 7, 5], [1, 2, 3, 7]) будет  масив [2, 3, 7]
let findClone = function(firstArr, secondArr) {
  firstArr = new Set(firstArr); 
  secondArr = new Set(secondArr);
  return [...firstArr].filter(clon => secondArr.has(clon));
};

console.log(findClone([1,6,10,12], [1,6,5,10,5,12]));

// Task 3
// Напишите функцию для объединения двух массивов и удаления всех дублирующих элементов.
const firstArray = [1, 2, 3, 16, 30, 42, 643, 95];
const secondArray = [2, 30, 1, 16, 42, 512, 95];
function mergeArray (firstArray, secondArray) {
	return Array.from(new Set([...firstArray, ...secondArray]));
}
const mergedArray = mergeArray(firstArray, secondArray);
console.log(mergedArray);

// Task 4
// Напишите функцию mapObject, которая преобразовывает значения
// всех свойств объекта (аналог map из массива, только для объекта, возвращает новый объект)
const double = x => x * 2;
function mapObject(fn, obj) {
  for (let i in obj) {
    if (obj.hasOwnProperty(i)) {
        obj[i] = double(obj[i]);
    }
  }
  
  return obj;
}
let obj = mapObject(double, {x: 1, y: 2, z: 3})
console.log(obj);
function request(url) {
    return new Promise((res, rej) => {
        const delayTime = Math.floor(Math.random() * 10000) + 1;
        setTimeout(() => res(url), delayTime);
    });
}
function willGetUrls(urls) {
    if (!urls || !urls.length) { return Promise.resolve([]); }
    return new Promise(function(resolve, reject) {
        var count = urls.length;
        var res = [];
        urls.forEach(function(url, urlIndex) {
            request(url).then(function(v) {
                res[urlIndex] = v;
                	if (--count < 1) { 
	                  resolve(res); 
	                  console.log(res)
	              	}
            });
        });
    });
}
const urls = [
    'look',
    'how',
    'does',
    'it',
    'work'
    ];
willGetUrls(urls);
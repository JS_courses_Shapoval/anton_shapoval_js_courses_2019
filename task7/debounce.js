function debounce(func, delay) {
    var timerId;
    return function() {
    	if (timerId){
            clearTimeout(timerId);
        }
        timerId = setTimeout(() => func.apply(this, [...arguments]), delay);
    }
}
function log(){
    console.log(...arguments);
}
var debouncedFirst = debounce(log, 500);
var debouncedSecond = debounce(log, 500);
var debouncedThird = debounce(log, 500);

debouncedFirst('1');
debouncedFirst('2');
debouncedSecond('3');
debouncedThird('4');
debouncedThird('5');
debouncedThird('6');